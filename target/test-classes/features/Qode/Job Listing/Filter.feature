Feature: Job Listing - Filter


  Scenario: Verify user filter job based on Workplace Type On-Site
    Given User open Qode World web browser
    When User click On-Site button
    Then User verify on-site job list shown


  Scenario: Verify user filter job based on Workplace Type Hybrid
    Given User open Qode World web browser
    When User click Hybrid button
    Then User verify 0 Result Found is shown

  Scenario: Verify user filter job based on Workplace Type Remote
    Given User open Qode World web browser
    When User click Remote button
    Then User verify remote job list shown

  Scenario: Verify user filter job based on Skills
    Given User open Qode World web browser
    When User input skill and select it
    Then User verify skill result filter is shown

  Scenario: Verify user filter job based on Job Type
    Given User open Qode World web browser
    When User click Full-Time button
    And User click first result list button
    Then User verify Full Time text

  Scenario: Verify user filter job based on Years of Experience
    Given User open Qode World web browser
    When User input Min and Max Years of Experience
    And User click first result list button
    Then User verify company result is min 5 Years YOE

  Scenario: Verify user filter job based on Company Industry
    Given User open Qode World web browser
    When User input fintech company industry and select it
    And User click first result list button
    Then User verify fintech company Industry result


  Scenario: Verify user filter job based on Working Time zone
    Given User open Qode World web browser
    When User input working time zone and select it
    And User click first result list button
    Then User verify show Vietnam Country

  Scenario: Verify user can reset all filtered variable
    Given User open Qode World web browser
    When User click On-Site button
    And User input skill and select it
    And User click Full-Time button
    And User input Min and Max Years of Experience
    And User input fintech company industry and select it
    And User input working time zone and select it
    Then User verify 0 Result Found is shown
    And User click Reset button
    Then User verify show 5 result