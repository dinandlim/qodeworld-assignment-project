package webdriver;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.touch.offset.PointOption;
import io.github.bonigarcia.wdm.WebDriverManager;
import net.serenitybdd.core.annotations.findby.By;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Utils {
    // Global Variables
    private static Properties PROPERTIES = SystemEnvironmentVariables.createEnvironmentVariables().getProperties();
    public static String currentTestCase;
    public static WebDriver device;
    public static String timeNow = null;
    public static String testName;
    public static int now;
    // ThreadLocal to handle parallel test execution
    public static final ThreadLocal<AndroidDriver<MobileElement>> threadLocal = new ThreadLocal<>();



    //mulai dari sini

    public static String getChromeDriverPath() {
        String OS = System.getProperty("os.name");
        String driverPath = System.getProperty("user.dir");
        if (OS.contains("Window")) {
            driverPath += "/webdriver/windows/chromedriver.exe";
        } else if (OS.contains("Mac")) {
            driverPath += "/webdriver/Mac/chromedriver";
        } else {
            driverPath += "/webdriver/Linux/chromedriver";
        }
        return driverPath;
    }

    public static WebDriver createLocalChromeDriver(WebDriver driver) {
        System.setProperty("webdriver.chrome.driver", Utils.getChromeDriverPath());
        if (driver != null) driver.quit();

        return new ChromeDriver();
    }

    public static WebDriver newWebDriver() throws MalformedURLException {

        WebDriverManager.chromedriver().setup();
        device = createLocalChromeDriver(device);
        device.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        device.manage().window().maximize();


        return device;
    }

    public static WebElement findAndWaitUntilWebElementPresent(String xPath) {
        try {
            WebElement webElement = device.findElement(By.xpath(xPath));
            WebDriverWait wait = new WebDriverWait(device, 30);
            wait.until(ExpectedConditions.visibilityOf(webElement));
            return webElement;
        } catch (Exception e) {
            // Log and print the error message
            System.err.println(e.getMessage());
            e.printStackTrace();

            // Capture the exception information for reporting
            try {
                ArrayList<String> exceptionCapture = new ArrayList<>();
                exceptionCapture.add(e.getMessage());
                // To print stack error msg
                StringWriter sw = new StringWriter();
                PrintWriter printWriter = new PrintWriter(sw);
                e.printStackTrace(printWriter);
                exceptionCapture.add(sw.toString());
            } catch (Exception ex) {
                // Skip if there's an issue with capturing the exception details
            }

            // Quit the device to ensure a clean state
            device.quit();
            return null;
        }
    }





}
