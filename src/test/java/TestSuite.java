import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/Qode",
        plugin = {"json:target/destination/cucumber7.json", "rerun:target/destination/rerun.txt"}
)
public class TestSuite {
}
