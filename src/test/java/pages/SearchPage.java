package pages;

import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import webdriver.Utils;

public class SearchPage extends PageObject {

    String inputSearchXPath = "//input[@id='react-select-search-input']";
    String textFullStackXPath = "//p[text()='Full Stack Engineer']";

    public void inputSearchDashboard() {
        WebElement textfieldSearch= Utils.device.findElement(By.xpath(inputSearchXPath));
        textfieldSearch.sendKeys("fullstack");
        waitABit(1000);
        textfieldSearch.sendKeys(Keys.ENTER);
    }

    public boolean verifyFullstackResult() {
        waitABit(2000);
        try {
            Utils.device.findElement(By.xpath(textFullStackXPath));
            return true;

        } catch (Exception e) {
            return false;
        }
    }

}
