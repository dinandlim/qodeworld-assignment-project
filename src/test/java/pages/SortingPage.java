package pages;

import io.appium.java_client.MobileElement;
import net.thucydides.core.pages.PageObject;
import org.jruby.RubyProcess;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import webdriver.Utils;

import java.util.ArrayList;
import java.util.List;

public class SortingPage extends PageObject {

    String BtnLast24HoursSortingXPath = "//p[text()='Last 24 hours']";
    String BtnLastWeekSortingXPath = "//p[text()='Last week']";
    String BtnTwoLastWeekSortingXPath = "//p[text()='Last 2 weeks']";
    String BtnLastMonthSortingXPath = "//p[text()='Last month']";
    String BtnAnytimeSortingXPath = "//p[text()='Anytime']";
    String BtnCloseJobDetailXPath = "//button[@aria-label='Close']";
    String BtnOldestSortXPath = "//p[text()='Oldest']";

    public void userClickLast24HoursSortingBtn() {
        WebElement btnLast24hoursSort = Utils.device.findElement(By.xpath(BtnLast24HoursSortingXPath));
        btnLast24hoursSort.click();
        waitABit(1000);
    }

    public void userClickLastWeekSortingBtn() {
        WebElement btnLastWeekSort = Utils.device.findElement(By.xpath(BtnLastWeekSortingXPath));
        btnLastWeekSort.click();
        waitABit(1000);
    }

    public void userClickLastTwoWeekSortingBtn() {
        WebElement btnLastWeekSort = Utils.device.findElement(By.xpath(BtnTwoLastWeekSortingXPath));
        btnLastWeekSort.click();
        waitABit(1000);
    }

    public void userClickLastMonthSortingBtn() {
        WebElement btnLastWeekSort = Utils.device.findElement(By.xpath(BtnLastMonthSortingXPath));
        btnLastWeekSort.click();
        waitABit(1000);
    }

    public void userClickOldestSortBtn() {
        WebElement btnOldestSort = Utils.device.findElement(By.xpath(BtnOldestSortXPath));
        btnOldestSort.click();
        waitABit(1000);
    }


    public void verifyJobListLatestSortingNotMoreThanSevenDays() {
        int totalJobListingResultShow = Utils.device.findElements(By.xpath("//div[@class='chakra-stack css-rozekk']")).size();
        List<String> Cont = new ArrayList<>();

        for (int i = 1; i <= totalJobListingResultShow; i++) {
            waitABit(1000);
            WebElement BtnJobList = Utils.device.findElement(By.xpath("//div[@class='chakra-stack css-rozekk'][" + i + "]"));
            BtnJobList.click();
            waitABit(1000);

            String result = "";
            WebElement textDay = Utils.device.findElement(By.xpath("//p[text()='Highlights']/../../../div[2]/div/div[2]/p"));
            result = textDay.getText();
            waitABit(2000);
            Cont.add(result);
            System.out.println(result);

            // Extract the numeric value
            String splitResult = result;
            String[] parts = splitResult.split(" ");
            String strDay = parts[0].replace(".", "");
            System.out.println(strDay);

            int intDay = Integer.parseInt(strDay);
            if (intDay <= 7) {
                System.out.println("Working good!! Day <= 7 days");
            }
            else{
                assert(false);
            }


            WebElement btnClose = Utils.device.findElement(By.xpath(BtnCloseJobDetailXPath));
            btnClose.click();

        }
        boolean sorted = false;
        for(int a = 0 ; a < Cont.size()-1 ; a++){
            if(Cont.get(a).compareTo(Cont.get(a+1))<=0){
                sorted = true;
            }
        }
        if(sorted){
            System.out.println("Sorted success");
        }
        else{
            assert(false);
        }
    }

    public void verifyJobListLatestSortingNotMoreThanTwoWeeks() {
        int totalJobListingResultShow = Utils.device.findElements(By.xpath("//div[@class='chakra-stack css-rozekk']")).size();
        List<String> Cont = new ArrayList<>();

        for (int i = 1; i <= totalJobListingResultShow; i++) {
            waitABit(1000);
            WebElement BtnJobList = Utils.device.findElement(By.xpath("//div[@class='chakra-stack css-rozekk'][" + i + "]"));
            BtnJobList.click();
            waitABit(1000);

            String result = "";
            WebElement textDay = Utils.device.findElement(By.xpath("//p[text()='Highlights']/../../../div[2]/div/div[2]/p"));
            result = textDay.getText();
            waitABit(2000);
            Cont.add(result);
            System.out.println(result);

            // Extract the numeric value
            String splitResult = result;
            String[] parts = splitResult.split(" ");
            String strDay = parts[0].replace(".", "");
            System.out.println(strDay);

            int intDay = Integer.parseInt(strDay);
            if (intDay <= 14) {
                System.out.println("Working good!! Day <= 14 days");
            }
            else{
                assert(false);
            }

            WebElement btnClose = Utils.device.findElement(By.xpath(BtnCloseJobDetailXPath));
            btnClose.click();

        }

        boolean sorted = false;
        for(int a = 0 ; a < Cont.size()-1 ; a++){
            if(Cont.get(a).compareTo(Cont.get(a+1))<=0){
                sorted = true;
            }
        }
        if(sorted){
            System.out.println("Sorted success");
        }
        else{
            assert(false);
        }

    }

    public void verifyJobListLatestSortingNotMoreThanOneMonth() {
        int totalJobListingResultShow = Utils.device.findElements(By.xpath("//div[@class='chakra-stack css-rozekk']")).size();
        List<String> Cont = new ArrayList<>();

        for (int i = 1; i <= totalJobListingResultShow; i++) {
            waitABit(1000);
            WebElement BtnJobList = Utils.device.findElement(By.xpath("//div[@class='chakra-stack css-rozekk'][" + i + "]"));
            BtnJobList.click();
            waitABit(1000);

            String result = "";
            WebElement textDay = Utils.device.findElement(By.xpath("//p[text()='Highlights']/../../../div[2]/div/div[2]/p"));
            result = textDay.getText();
            waitABit(2000);
            Cont.add(result);
            System.out.println(result);


            String splitResult = result;
            String[] parts = splitResult.split(" ");
            String strDay = parts[0].replace(".", "");
            System.out.println(strDay);

            int intDay = Integer.parseInt(strDay);
            if (intDay <= 30) {
                System.out.println("Working good!! Day <= 30 days");
            }
            else{
                assert(false);
            }

            WebElement btnClose = Utils.device.findElement(By.xpath(BtnCloseJobDetailXPath));
            btnClose.click();

        }

        boolean sorted = false;
        for(int a = 0 ; a < Cont.size()-1 ; a++){
            if(Cont.get(a).compareTo(Cont.get(a+1))<=0){
                sorted = true;
            }
        }
        if(sorted){
            System.out.println("Sorted success");
        }
        else{
            assert(false);
        }

    }

    public void verifyJobListOldestSortingNotMoreThanSevenDays() {
        int totalJobListingResultShow = Utils.device.findElements(By.xpath("//div[@class='chakra-stack css-rozekk']")).size();
        List<String> Cont = new ArrayList<>();

        for (int i = 1; i <= totalJobListingResultShow; i++) {
            waitABit(1000);
            WebElement BtnJobList = Utils.device.findElement(By.xpath("//div[@class='chakra-stack css-rozekk'][" + i + "]"));
            BtnJobList.click();
            waitABit(1000);

            String result = "";
            WebElement textDay = Utils.device.findElement(By.xpath("//p[text()='Highlights']/../../../div[2]/div/div[2]/p"));
            result = textDay.getText();
            waitABit(2000);
            Cont.add(result);
            System.out.println(result);

            // Extract the numeric value
            String splitResult = result;
            String[] parts = splitResult.split(" ");
            String strDay = parts[0].replace(".", "");
            System.out.println(strDay);

            int intDay = Integer.parseInt(strDay);
            if (intDay <= 7) {
                System.out.println("Working good!! Day <= 7 days");
            }
            else{
                assert(false);
            }


            WebElement btnClose = Utils.device.findElement(By.xpath(BtnCloseJobDetailXPath));
            btnClose.click();

        }
        boolean sorted = false;
        for(int a = 0 ; a < Cont.size()-1 ; a++){
            if(Cont.get(a).compareTo(Cont.get(a+1))>=0){
                sorted = true;
            }
        }
        if(sorted){
            System.out.println("Sorted success");
        }
        else{
            assert(false);
        }
    }

    public void verifyJobListOldestSortingNotMoreThanTwoWeeks() {
        int totalJobListingResultShow = Utils.device.findElements(By.xpath("//div[@class='chakra-stack css-rozekk']")).size();
        List<String> Cont = new ArrayList<>();

        for (int i = 1; i <= totalJobListingResultShow; i++) {
            waitABit(1000);
            WebElement BtnJobList = Utils.device.findElement(By.xpath("//div[@class='chakra-stack css-rozekk'][" + i + "]"));
            BtnJobList.click();
            waitABit(1000);

            String result = "";
            WebElement textDay = Utils.device.findElement(By.xpath("//p[text()='Highlights']/../../../div[2]/div/div[2]/p"));
            result = textDay.getText();
            waitABit(2000);
            Cont.add(result);
            System.out.println(result);

            // Extract the numeric value
            String splitResult = result;
            String[] parts = splitResult.split(" ");
            String strDay = parts[0].replace(".", "");
            System.out.println(strDay);

            int intDay = Integer.parseInt(strDay);
            if (intDay <= 14) {
                System.out.println("Working good!! Day <= 14 days");
            }
            else{
                assert(false);
            }

            WebElement btnClose = Utils.device.findElement(By.xpath(BtnCloseJobDetailXPath));
            btnClose.click();

        }

        boolean sorted = false;
        for(int a = 0 ; a < Cont.size()-1 ; a++){
            if(Cont.get(a).compareTo(Cont.get(a+1))>=0){
                sorted = true;
            }
        }
        if(sorted){
            System.out.println("Sorted success");
        }
        else{
            assert(false);
        }

    }

    public void verifyJobListOldestSortingNotMoreThanOneMonth() {
        int totalJobListingResultShow = Utils.device.findElements(By.xpath("//div[@class='chakra-stack css-rozekk']")).size();
        List<String> Cont = new ArrayList<>();

        for (int i = 1; i <= totalJobListingResultShow; i++) {
            waitABit(1000);
            WebElement BtnJobList = Utils.device.findElement(By.xpath("//div[@class='chakra-stack css-rozekk'][" + i + "]"));
            BtnJobList.click();
            waitABit(1000);

            String result = "";
            WebElement textDay = Utils.device.findElement(By.xpath("//p[text()='Highlights']/../../../div[2]/div/div[2]/p"));
            result = textDay.getText();
            waitABit(2000);
            Cont.add(result);
            System.out.println(result);


            String splitResult = result;
            String[] parts = splitResult.split(" ");
            String strDay = parts[0].replace(".", "");
            System.out.println(strDay);

            int intDay = Integer.parseInt(strDay);
            if (intDay <= 30) {
                System.out.println("Working good!! Day <= 30 days");
            }
            else{
                assert(false);
            }

            WebElement btnClose = Utils.device.findElement(By.xpath(BtnCloseJobDetailXPath));
            btnClose.click();

        }

        boolean sorted = false;
        for(int a = 0 ; a < Cont.size()-1 ; a++){
            if(Cont.get(a).compareTo(Cont.get(a+1))>=0){
                sorted = true;
            }
        }
        if(sorted){
            System.out.println("Sorted success");
        }
        else{
            assert(false);
        }

    }
}





