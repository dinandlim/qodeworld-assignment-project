package pages;

import net.bytebuddy.asm.Advice;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import webdriver.Utils;


public class FilterPage extends PageObject {

    String btnOnSiteXPath = "//span[text()='On-Site']";
    String txtOnSiteXPath = "//p[text()='On-site']";
    String btnHybridXPath = "//span[text()='Hybrid']";
    String btnRemoteXPath = "//span[text()='Remote']";
    String txtRemoteXPath = "//p[text()='Remote']";
    String txtZeroResultXPath = "//p[text()='0 Result Found']";
    String btnSkillXPath = "//div[@id='react-select-skill-placeholder']";
    String inputSkillXPath = "//input[@id='react-select-skill-input']";
    String txtSkillJavaResultXPath = "//p[contains(.,'Result(s) Found')]/following-sibling::div/div[2]/div/span/span[text()='Java']";
    String btnFullTimeXPath = "//span[text()='Full-time']";
    String btnFirstResultListXPath = "//p[contains(.,'Result(s) Found')]/following-sibling::div";
    String inputMinYOEXPath = "//input[@placeholder='Min']";
    String inputMaxYOEXPath = "//input[@placeholder='Max']";
    String textCompanyResultFiveYOE = "//p[text()='5' and text()='+ years of experience']";
    String inputCompanyIndustryXPath = "//input[@id='react-select-company_industry-input']";
    String inputWorkingTimeZoneXPath = "//input[@id='react-select-timezone-input']";
    String getTextCompanyIndustryResultXPath = "//div/p[contains(.,'FinTech & Financial Services')]";
    String textVietnamCountryXPath = "//p[text()='Vietnam']";
    String btnResetXPath = "//p[text()='Reset']";
    String btnShowFiveResultXPath = "//p[text()='5' and text()=' Result(s) Found']";


    public void userClickOnSiteBtn() {
        WebElement btnSearch = Utils.device.findElement(By.xpath(btnOnSiteXPath));
        btnSearch.click();
        waitABit(1000);
    }

    public void userClickHybridBtn() {
        WebElement btnSearch = Utils.device.findElement(By.xpath(btnHybridXPath));
        btnSearch.click();
        waitABit(1000);
    }

    public void userClickRemoteBtn() {
        WebElement btnSearch = Utils.device.findElement(By.xpath(btnRemoteXPath));
        btnSearch.click();
        waitABit(1000);
    }

    public void userClickSkillBtn() {
        WebElement btnSkill= Utils.device.findElement(By.xpath(btnSkillXPath));
        btnSkill.click();
        waitABit(1000);
    }

    public void userTypeSkillText() {
        WebElement btnSkill= Utils.device.findElement(By.xpath(inputSkillXPath));
        btnSkill.sendKeys("Java");
        waitABit(1000);
        btnSkill.sendKeys(Keys.ENTER);
    }


    public boolean verifyOnSiteText() {
        waitABit(2000);
        try {
            Utils.device.findElement(By.xpath(txtOnSiteXPath));
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public void verifyOnSiteJobList() {
        int totalJobListingResultShow = Utils.device.findElements(By.xpath("//p[text()='On-site']")).size();
        System.out.println(totalJobListingResultShow);

        for (int i = 1; i <= totalJobListingResultShow; i++) {
            waitABit(1000);
            Utils.device.findElement(By.xpath("//div["+i+"]/div/div/div/div/p[text()='On-site']"));
            System.out.println("On-site job list shown");
            waitABit(1000);
        }
    }

    public void verifyRemoteJobList() {
        int totalJobListingResultShow = Utils.device.findElements(By.xpath("//p[text()='Remote']")).size();
        System.out.println(totalJobListingResultShow);

        for (int i = 1; i <= totalJobListingResultShow; i++) {
            waitABit(1000);
            Utils.device.findElement(By.xpath("//div["+i+"]/div/div/div/div/p[text()='Remote']"));
            System.out.println("Remote job list shown");
            waitABit(1000);
        }
    }

    public boolean verifyRemoteText() {
        waitABit(2000);
        try {
            Utils.device.findElement(By.xpath(txtRemoteXPath));
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public boolean verifyShowFilterSkillResult() {
        waitABit(2000);
        try {
            Utils.device.findElement(By.xpath(txtSkillJavaResultXPath));
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public boolean verifyNotSeeOnSiteText() {
        waitABit(2000);
        try {
            Utils.device.findElement(By.xpath(txtOnSiteXPath));
            return false;

        } catch (Exception e) {
            return true;
        }
    }

    public boolean verifyNotSeeRemoteText() {
        waitABit(2000);
        try {
            Utils.device.findElement(By.xpath(txtRemoteXPath));
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public boolean verifyZeroResultText() {
        waitABit(2000);
        try {
            Utils.device.findElement(By.xpath(txtZeroResultXPath));
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public void userClickFullTimeBtn() {
        WebElement btnFullTime = Utils.device.findElement(By.xpath(btnFullTimeXPath));
        btnFullTime.click();
        waitABit(1000);
    }

    public void userClickFirstResultListBtn() {
        WebElement btnFirstResultList = Utils.device.findElement(By.xpath(btnFirstResultListXPath));
        btnFirstResultList.click();
        waitABit(1000);
    }

    public boolean verifyFullTimeText() {
        waitABit(2000);
        try {
            Utils.device.findElement(By.xpath(btnFullTimeXPath));
            return true;

        } catch (Exception e) {
            return false;
        }
    }


    public void userInputMinAndMaxYOE() {
        WebElement inputMinYOE = Utils.device.findElement(By.xpath(inputMinYOEXPath));
        inputMinYOE.sendKeys("5");
        waitABit(1000);
        WebElement inputMaxYOE = Utils.device.findElement(By.xpath(inputMaxYOEXPath));
        inputMaxYOE.sendKeys("5");
        waitABit(1000);

    }

    public boolean verifyCompanyResultFiveYOE() {
        waitABit(2000);
        try {
            Utils.device.findElement(By.xpath(textCompanyResultFiveYOE));
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public void inputFintechCompanyIndustry() {
        WebElement textCompanyIndustry= Utils.device.findElement(By.xpath(inputCompanyIndustryXPath));
        textCompanyIndustry.sendKeys("fintech");
        waitABit(1000);
        textCompanyIndustry.sendKeys(Keys.ENTER);
        waitABit(1000);
    }

    public boolean verifyCompanyIndustryResult() {
        waitABit(2000);
        try {
            Utils.device.findElement(By.xpath(getTextCompanyIndustryResultXPath));
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public void inputWorkingTimeZoneUTCPlus07() {
        WebElement textCompanyIndustry= Utils.device.findElement(By.xpath(inputWorkingTimeZoneXPath));
        textCompanyIndustry.sendKeys("07");
        waitABit(1000);
        textCompanyIndustry.sendKeys(Keys.DOWN);
        textCompanyIndustry.sendKeys(Keys.ENTER);
        waitABit(1000);
    }

    public boolean verifyVietnamCountryShow() {
        waitABit(2000);
        try {
            Utils.device.findElement(By.xpath(textVietnamCountryXPath));
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public void userClickResetBtn() {
        WebElement btnReset = Utils.device.findElement(By.xpath(btnResetXPath));
        btnReset.click();
        waitABit(1000);
    }

    public boolean verifyFiveResultFound() {
        waitABit(2000);

        //int totalJobListingResultShow = Utils.device.findElements(By.xpath("//div[@class='chakra-stack css-rozekk']")).size();

        //WebElement txtResult = Utils.device.findElement(By.id(btnShowFiveResultXPath));
       // txtResult.getText();



        try {
            Utils.device.findElement(By.xpath(btnShowFiveResultXPath));
            return true;

        } catch (Exception e) {
            return false;
        }

    }






}
