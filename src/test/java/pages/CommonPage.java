package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import webdriver.Utils;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.*;
import java.util.List;

public class CommonPage {

    private void waitABit(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            // Handle any exception if needed
            e.printStackTrace();
        }
    }

    public void userClick(String text) {

        WebElement webElement = Utils.device.findElement(By.xpath("//p[text()='"+text+"]"));


        //waitFor(webElement);
        waitABit(2000);
        webElement.click();
        waitABit(2000);
    }

    public boolean textIsVisible(String text) {
        WebElement anyText = Utils.findAndWaitUntilWebElementPresent("//body");
        return anyText.getText().contains(text);
    }


}
