package pages;

import net.thucydides.core.pages.PageObject;
import webdriver.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.net.MalformedURLException;

public class OpenBrowserPage extends PageObject {

    public static void pleaseWaitFor(int i) {
        try {
            Thread.sleep(i);
        } catch (Exception e) {
            // Log or handle any exceptions
        }
    }


    public void openQodeWorld() throws MalformedURLException {
        Utils.device = Utils.newWebDriver();
        pleaseWaitFor(2000);
        Utils.device.get("https://qode.world/career/job-listing");
        pleaseWaitFor(5000);
    }
}
