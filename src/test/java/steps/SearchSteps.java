package steps;

import cucumber.api.java.en.And;
import org.junit.Assert;
import pages.SearchPage;

public class SearchSteps {

    SearchPage searchPage;

    @And("^User input fullstack in search textfield$")
    public void userInputSearchDashboard() {
        searchPage.inputSearchDashboard();
    }

    @And("^User verify fullstack result$")
    public void userVerifyRemoteListShown() {
        boolean isVisible = searchPage.verifyFullstackResult();
        Assert.assertTrue("Show remote list", isVisible);
    }
}
