package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.jruby.RubyProcess;
import org.junit.Assert;
import pages.CommonPage;
import pages.FilterPage;

public class FilterSteps {
    CommonPage commonPage;
    FilterPage filterPage;



    @And("^User click On-Site button$")
    public void userClickOnSiteButton() {
        filterPage.userClickOnSiteBtn();
    }

    @And("^User click Hybrid button$")
    public void userClickHybridButton() {
        filterPage.userClickHybridBtn();
    }

    @And("^User click Remote button$")
    public void userClickRemoteButton() {
        filterPage.userClickRemoteBtn();
    }

    @And("^User verify on-site list shown$")
    public void userVerifyOnSiteListShown() {
        boolean isVisible = filterPage.verifyOnSiteText();
        Assert.assertTrue("Show on-site list", isVisible);
    }

    @And("^User verify on-site job list shown$")
    public void userVerifyOnSiteJobListShown() {
        filterPage.verifyOnSiteJobList();
    }

    @And("^User verify remote list shown$")
    public void userVerifyRemoteListShown() {
        boolean isVisible = filterPage.verifyRemoteText();
        Assert.assertTrue("Show remote list", isVisible);
    }

    @And("^User verify remote job list shown$")
    public void userVerifyRemoteJobListShown() {
        filterPage.verifyRemoteJobList();
    }

    @And("^User verify On-Site list not shown$")
    public void userVerifyOnSiteListNotShown() {
        boolean isNotVisible = filterPage.verifyNotSeeOnSiteText();
        Assert.assertTrue("On-Site list not shown", isNotVisible);
    }

    @And("^User verify remote list not shown$")
    public void userVerifyRemoteListNotShown() {
        boolean isNotVisible = filterPage.verifyNotSeeRemoteText();
        Assert.assertTrue("remote list not shown", isNotVisible);
    }

    @And("^User verify 0 Result Found is shown$")
    public void userVerifyZeroResultShown() {
        boolean isVisible = filterPage.verifyZeroResultText();
        Assert.assertTrue("remote list not shown", isVisible);
    }

    @And("^User click Skill filter button$")
    public void userClickSkillFilterButton() {
        filterPage.userClickSkillBtn();
    }

    @And("^User input skill and select it$")
    public void userInputSkill() {
        filterPage.userTypeSkillText();
    }

    @And("^User verify skill result filter is shown$")
    public void userVerifyResultSkillFilter() {
        boolean isVisible = filterPage.verifyShowFilterSkillResult();
        Assert.assertTrue("Skill Filter Java is shown", isVisible);
    }

    @And("^User click Full-Time button$")
    public void userClickFullTimeButton() {
        filterPage.userClickFullTimeBtn();
    }

    @And("^User click first result list button$")
    public void userClickFirstResultList() {
        filterPage.userClickFirstResultListBtn();
    }

    @And("^User verify Full Time text$")
    public void userVerifyFullTimeText() {
        boolean isNotVisible = filterPage.verifyFullTimeText();
        Assert.assertTrue("Full Time text shown", isNotVisible);
    }

    @When("^User input Min and Max Years of Experience$")
    public void userInputMinAndMaxYOE() {
        filterPage.userInputMinAndMaxYOE();
    }

    @Then("^User verify company result is min 5 Years YOE$")
    public void userVerifyCompanyResultFiveYOE() {
        boolean isVisible = filterPage.verifyCompanyResultFiveYOE();
        Assert.assertTrue("Company Result is Min 5 YOE", isVisible);
    }

    @When("^User input fintech company industry and select it$")
    public void userInputFintechCompanyIndustry() {
        filterPage.inputFintechCompanyIndustry();
    }

    @Then("^User verify fintech company Industry result$")
    public void userVerifyFintechCompanyIndustryResult() {
        boolean isVisible = filterPage.verifyCompanyIndustryResult();
        Assert.assertTrue("Fintech Company Industry result is shown", isVisible);
    }

    @When("^User input working time zone and select it$")
    public void userInputWorkingTimeZone() {
        filterPage.inputWorkingTimeZoneUTCPlus07();
    }

    @Then("^User verify show Vietnam Country$")
    public void userVerifyShowVietnamCountry() {
        boolean isVisible = filterPage.verifyVietnamCountryShow();
        Assert.assertTrue("Show Vietnam Country UTC+07", isVisible);
    }

    @Then("^User click Reset button$")
    public void userClickResetButton() {
        filterPage.userClickResetBtn();
    }

    @Then("^User verify show 5 result$")
    public void userVerifyShowFiveResult() {
        boolean isVisible = filterPage.verifyFiveResultFound();
        Assert.assertTrue("Show Five Result shown", isVisible);
    }






}
