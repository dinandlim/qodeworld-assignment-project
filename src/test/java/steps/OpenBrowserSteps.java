package steps;

import cucumber.api.java.en.Given;
import pages.OpenBrowserPage;

import static org.junit.Assert.assertTrue;

public class OpenBrowserSteps {

    OpenBrowserPage openbrowserPage;


    @Given("^User open Qode World web browser$")
    public void userOpenQodeWorld() {
        try {
            openbrowserPage.openQodeWorld();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //assertTrue(openbrowserPage.isLoginFormVisible());
    }


}
