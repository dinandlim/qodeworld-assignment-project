package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import pages.CommonPage;

import static org.junit.Assert.assertTrue;

public class CommonSteps {

    CommonPage commonPage;

    @Then("^User could see '(.*)' in webpage$")
    public void userCouldSeeGivenTextInPage(String text) {
        boolean isVisible = commonPage.textIsVisible(text);
        assertTrue("Searched text is not found!", isVisible);
    }

    @And("^User click '(.*)'$")
    public void userClick(String text) {
        commonPage.userClick(text);
    }

}
