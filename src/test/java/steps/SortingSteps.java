package steps;

import cucumber.api.java.en.And;
import pages.SortingPage;

public class SortingSteps {

    SortingPage sortingPage;

    @And("^User click Last 24 Hours Sorting button$")
    public void userClickLast24HoursSortingButton() {
        sortingPage.userClickLast24HoursSortingBtn();
    }

    @And("^User click Last Week Sorting button$")
    public void userClickLastWeekSortingBtn() {
        sortingPage.userClickLastWeekSortingBtn();
    }

    @And("^User click Last 2 Week Sorting button$")
    public void userClickLasTwoWeekSortingBtn() {
        sortingPage.userClickLastTwoWeekSortingBtn();
    }

    @And("^User click Last Month Sorting button$")
    public void userClickLastMonthSortingBtn() {
        sortingPage.userClickLastMonthSortingBtn();
    }

    @And("^User verify job list Latest Sorting not more than 7 days$")
    public void userVerifyJobListNotMoreThanSevenDays() {
        sortingPage.verifyJobListLatestSortingNotMoreThanSevenDays();
    }

    @And("^User verify job list Latest Sorting not more than 2 weeks$")
    public void userVerifyJobListNotMoreThanTwoWeeks() {
        sortingPage.verifyJobListLatestSortingNotMoreThanTwoWeeks();
    }

    @And("^User verify job list Latest Sorting not more than 1 month$")
    public void userVerifyJobListNotMoreThanOneMonth() {
        sortingPage.verifyJobListLatestSortingNotMoreThanOneMonth();
    }

    @And("^User click Oldest Sorting button$")
    public void userClickOldestSortingBtn() {
        sortingPage.userClickOldestSortBtn();
    }

    @And("^User verify job list Oldest Sorting not more than 7 days$")
    public void userVerifyJobListOldestSortingNotMoreThanSevenDays() {
        sortingPage.verifyJobListLatestSortingNotMoreThanSevenDays();
    }

    @And("^User verify job list Oldest Sorting not more than 2 weeks$")
    public void userVerifyJobListOldestSortingNotMoreThanTwoWeeks() {
        sortingPage.verifyJobListLatestSortingNotMoreThanTwoWeeks();
    }

    @And("^User verify job list Oldest Sorting not more than 1 month$")
    public void userVerifyJobListOldestSortingNotMoreThanOneMonth() {
        sortingPage.verifyJobListLatestSortingNotMoreThanOneMonth();
    }
}
