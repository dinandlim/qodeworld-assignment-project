Feature: Job Listing - Sorting


  Scenario: Verify user job Latest sorting Last 24 hours
    Given User open Qode World web browser
    When User click Last 24 Hours Sorting button
    Then User verify 0 Result Found is shown

  Scenario: Verify user job Latest sorting Last Week
    Given User open Qode World web browser
    When User click Last Week Sorting button
    Then User verify job list Latest Sorting not more than 7 days

  Scenario: Verify user job Latest sorting Last 2 Week
    Given User open Qode World web browser
    When User click Last 2 Week Sorting button
    Then User verify job list Latest Sorting not more than 2 weeks

  Scenario: Verify user job Latest sorting Last Month
    Given User open Qode World web browser
    When User click Last Month Sorting button
    Then User verify job list Latest Sorting not more than 1 month

  Scenario: Verify user job Oldest sorting Last 24 hours
    Given User open Qode World web browser
    When User click Oldest Sorting button
    And User click Last 24 Hours Sorting button
    Then User verify 0 Result Found is shown

  Scenario: Verify user job Oldest sorting Last Week
    Given User open Qode World web browser
    When User click Oldest Sorting button
    And User click Last Week Sorting button
    Then User verify job list Oldest Sorting not more than 7 days

  Scenario: Verify user job Oldest sorting Last 2 Week
    Given User open Qode World web browser
    When User click Oldest Sorting button
    And User click Last 2 Week Sorting button
    Then User verify job list Oldest Sorting not more than 2 weeks

  Scenario: Verify user job Oldest sorting Last Month
    Given User open Qode World web browser
    When User click Oldest Sorting button
    And User click Last Month Sorting button
    Then User verify job list Oldest Sorting not more than 1 month

